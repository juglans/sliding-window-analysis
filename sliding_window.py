import argparse
from nipype import Node, Workflow, Function
from nipype.interfaces.utility import Merge as niMerge
from nipype.interfaces.fsl import Merge as FSLMerge
from nipype.interfaces.fsl.utils import ExtractROI
from nipype.interfaces.fsl import ImageMeants, GLM
from nibabel import load, Nifti1Image
import numpy as np
import os
from sys import exit

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('input_flist', help='Path of input file list, must be newline delimited text file with one file path to a .nii or .nii.gz file on each line.')
    parser.add_argument('seed', help='Path to the desired seed mask image. If the image is not binary it will be assumed that all nonzero voxels in the mask image are desired for timeseries extraction.')
    parser.add_argument('--output_path', help='Path to desired output directory. If the path does not exist it will be created.')
    parser.add_argument('--slidingstep', type=int, default=1, help='Number of TRs to increment the sliding windows.')
    parser.add_argument('--slidingwindow', type=int, default=18, help='Number of TRs to include in each sliding window constituent analysis.')
    parser.add_argument('--tstat', action='store_true', default=False, help='Output and concatenate t-statistics from sliding window analysis.')
    parser.add_argument('--zstat', action='store_true', default=True, help='Output and concatenate z-statistics from sliding window analysis.')
    parser.add_argument('--bstat', action='store_true', default=False, help='Output and concatenate betas (parameter estimates) from sliding window analysis.')
    parser.add_argument('--workingdir', type=str, default='/tmp', help='Working directory to store temp nipype workflow files and intermediates. Defaults to /tmp or TMPDIR (if created).')
    parser.add_argument('--n-procs', type=int, default=2, help='Number of processes (not threads) to use for the MultiProcessing.')

    args = parser.parse_args()
   
    if os.path.exists(args.input_flist) is False:
        print('Input file [{}] does not exist. Exiting.'.format(args.input_flist))
        exit(1)
        
    if not args.output_path:
        print('Output directory path was not specified, writing outputs to ./sliding_window_analysis/')
        args.output_path = os.path.join(os.getcwd(), 'sliding_window_analysis')
    else:
        args.output_path = os.path.abspath(args.output_path)

    if os.path.isdir(args.output_path) is False:
        print('Creating output directory: {}'.format(args.output_path))
        os.makedirs(args.output_path, exist_ok=True)

    
    
    return args

def isnii(nii_file):
    """
    Helper function to determine whether a file can be loaded using nibabel successfully.
    Arguments:
        nii_file {str} -- string path
    
    Returns:
        [boolean] -- Boolean value for whether the file can be opened with nibabel's load function.
    """


    try:
        load(nii_file)
        return True
    except TypeError:
        print('Could not open {} using nibabel. Check for file integrity? This file will be skipped.')
        return False
    except FileNotFoundError:
        print('Could not find {}. This file will be skipped.')
        return False

def get_binarized_seed(seedpath):
    """
    Helper function to make sure that a given seed mask is binarized. 
    If the seedpath is binary (0 or 1 throughout) then the seedpath will be returned. 
    If not, a binarized file with 0 for non-mask and 1 for nonzero voxels (all of them) will be created and the path to that file returned.
    Arguments:
        seedpath {[str]} -- Path to seed.
    
    Returns:
        [str] -- Path to seed, or path to new file that has been binarized if the original file was not found to be binary.
    """

    print('Checking if seed is binarized.')
    nii_obj  = load(seedpath)
    nii_data = nii_obj.get_fdata()
    
    num_zero_or_one_vox = np.count_nonzero((nii_data == 0) | (nii_data == 1))
    if num_zero_or_one_vox == nii_data.size:
        print('Seed is binary.')
        return seedpath
    else:
        binarized = nii_data.copy() 
        binarized[binarized > 0] = 1

        seed_dirname, seed_basename = os.path.split(seedpath)
        fstem = seed_basename.split('.')[0] 
        binarized_seed_basename = seed_basename + '_binarized.nii.gz'
        binarized_seed_fullpath = os.path.join(seed_dirname, binarized_seed_basename)

        print('Seed is not binary. Converting to binary nii.gz file @ {}'.format(binarized_seed_fullpath))

        Nifti1Image(binarized, nii_obj.affine, nii_obj.header).to_filename(binarized_seed_fullpath)

        print('Finished writing binarized seed file.')
        
        return binarized_seed_fullpath

def merge_save(nii_paths, output_path):
    """
    Function to merge the results of different sliding-window sub analyses into one large 4D nii file.
    Arguments:
        nii_paths {[str]} -- Paths of sub-analysis output files (betas, tstats, etc.).
        output_path {[str]} -- Desired output path of merged file.

    Returns:
        [str] -- Path to merged file.
    """
    from nibabel import concat_images
    concatenated_image = concat_images(nii_paths)
    concatenated_image.to_filename(output_path)
    return output_path

def parse_input_files(input_flist):
    """
    Helper function to parse a newline delimited file containing a list of files for analysis.  
    The files will each be passed to isnii() to check if they can be loaded with nibabel.
    Arguments:
        input_flist {[str]} -- Path to input file.
    
    Returns:
        [list] -- List of file paths that can be loaded with nibabel. 
    """
    nii_paths = []
    with open(input_flist, 'r') as f:
        for lineno, line in enumerate(f):
            stripped_line = line.strip()
            if isnii(stripped_line) is False:
                pass
            else:
                nii_paths.append(stripped_line)
    if len(nii_paths) == 0:
        print('No valid nii files were located. Exiting.')
        exit(1)

    print('Parsed input file. {} lines were counted, and {} filepaths were extracted.'.format(lineno, len(nii_paths)))
    return nii_paths

def get_sliding_window_indices(total_num_vols, window_step, window_size):
    """
    Given a desired step size and number of TRs before window, return two lists, one that are the indices corresponding to the first TR in each window, and another for the length.
    These lists can be passed to fsl_roi to generate the BOLD 4D windows of interest for the analysis.
    Arguments:
        total_num_vols {int} -- Total number of TRs.
        window_step {int} -- Number of TRs to increment the sliding window for each sub-analysis.
        window_size {int} -- Number of TRs per window.
    
    Returns:
        [list] -- List of window start TR indices.
        [list] -- List of window lengths, will be [window_size, window_size, ...] up the number of desired windows.
    """
    window_start_indices = []
    for idx in range(0, total_num_vols, window_step):
        if idx + window_size > total_num_vols:
            break
        window_start_indices.append(idx)
    return window_start_indices, [window_size]*len(window_start_indices)

def generate_sliding_window_map(nii_file, seed, slidingstep, slidingwindow, base_dir, output_dir, betas=False, tstat=False, zstat=True, identifier=None, seedname=None):
    """
    Function to return a nipype workflow for each sliding window analysis.
    Arguments:
        nii_file {str} -- Path to input 4D timeseries nii file.
        seed {str} -- Path to seed mask.
        slidingstep {int} -- Number of TRs to increment the sliding window for each sub-analysis.
        slidingwindow {int} -- Number of TRs per window.
        base_dir {str} -- String path to the desired working directory. By default this will be set to /tmp
        output_dir {str} -- Desired output directory path.
    
    Keyword Arguments:
        betas {bool} -- Flag return of betas (parameter estimates). (default: {False})
        tstat {bool} -- Flag return of tstats. (default: {False})
        zstat {bool} -- Flag return of zstats. (default: {True})
        identifier {str} -- String to append to each working dir to make it easier to identify nodes. (default: {None})
        seedname {str} -- Name of seed. (default: {None})
    
    Returns:
        [nipype.Workflow] -- Nipype workflow that can be run individually or attached to a larger workflow.
    """

    wkflow_name = os.path.basename(nii_file).split('.')[0]
    nii_stem = wkflow_name
    node_prefix = wkflow_name
    if identifier is not None: 
        identifier = '_' + identifier
        wkflow_name = wkflow_name + '_' + identifier
    else: identifier = ''

    if seedname is None:
        seedname = os.path.basename(seed)

    workflow = Workflow(name=os.path.basename(nii_file).split('.')[0], base_dir=base_dir)
    
    nii_obj = load(nii_file)
    x, y, z, numvols = nii_obj.shape
    tr = nii_obj.header['pixdim'][3]

    sliding_start_indices, window_lengths = get_sliding_window_indices(numvols, slidingstep, slidingwindow)
    num_windows = len(sliding_start_indices)
    print(num_windows)
    print(sliding_start_indices, window_lengths)

    if betas:
        betas_merged_file = os.path.join(output_dir, nii_stem + '_betas_dynamic-step_{}-window_{}_{}'.format(slidingstep, slidingwindow, seedname))
        betas_nipype_merge_node = Node(niMerge(num_windows), name=node_prefix + '_BetaWindowCollectorNode' + identifier)
        betas_file_merge_node = Node(Function(function=merge_save, input_names=['nii_paths', 'output_path'], output_names=['output_path']), output_path=betas_merged_file, name=node_prefix + '_betasFinalMergeNode' + identifier)
        betas_file_merge_node.inputs.output_path = betas_merged_file

    if tstat:
        tstat_merged_file = os.path.join(output_dir, nii_stem + '_tstat_dynamic-step_{}-window_{}_{}'.format(slidingstep, slidingwindow, seedname))
        tstat_nipype_merge_node = Node(niMerge(num_windows), name=node_prefix + '_tstatWindowCollectorNode' + identifier)
        tstat_file_merge_node = Node(Function(function=merge_save, input_names=['nii_paths', 'output_path'], output_names=['output_path']), output_path=tstat_merged_file, name=node_prefix + '_tstatFinalMergeNode' + identifier)
        tstat_file_merge_node.inputs.output_path = tstat_merged_file

    if zstat:
        zstat_merged_file = os.path.join(output_dir, nii_stem + '_zstat_dynamic-step_{}-window_{}_{}.nii.gz'.format(slidingstep, slidingwindow, seedname))
        zstat_nipype_merge_node = Node(niMerge(num_windows), name=node_prefix + '_zstatWindowCollectorNode' + identifier)
        zstat_file_merge_node = Node(Function(function=merge_save, input_names=['nii_paths', 'output_path'], output_names=['output_path']), output_path=zstat_merged_file, name=node_prefix + '_zstatFinalMergeNode' + identifier)
        zstat_file_merge_node.inputs.output_path = zstat_merged_file

    for sliding_window_index, windowparams in enumerate(zip(sliding_start_indices, window_lengths)):
        window_start, window_length = windowparams

        out_beta = nii_stem + '_window_{}_beta.nii.gz'.format(str(sliding_window_index).zfill(3))
        out_z = nii_stem + '_window_{}_zstat.nii.gz'.format(str(sliding_window_index).zfill(3))
        out_t = nii_stem + '_window_{}_tstat.nii.gz'.format(str(sliding_window_index).zfill(3))

        slicer_node = Node(ExtractROI(in_file=nii_file, t_min=window_start, t_size=window_length), name=node_prefix + '_SlicerNum{}'.format(str(sliding_window_index).zfill(3) + identifier))
        sliding_ts_extract_node = Node(ImageMeants(mask=seed), name=node_prefix + '_TSExtractNum{}'.format(str(sliding_window_index).zfill(3) + identifier))
        sliding_glm_node = Node(GLM(out_file=out_beta, out_z_name=out_z, out_t_name=out_t), name=node_prefix + '_GLMNum{}'.format(str(sliding_window_index).zfill(3) + identifier))

        workflow.connect([
            (slicer_node, sliding_ts_extract_node, [('roi_file', 'in_file')]),
            (slicer_node, sliding_glm_node, [('roi_file', 'in_file')]),
            (sliding_ts_extract_node, sliding_glm_node, [('out_file', 'design')]),
            ])

        if betas:
            workflow.connect([(sliding_glm_node, betas_nipype_merge_node, [('out_file', 'in{}'.format(sliding_window_index+1))])])
        if tstat:
            workflow.connect([(sliding_glm_node, tstat_nipype_merge_node, [('out_t', 'in{}'.format(sliding_window_index+1))])])
        if zstat:
            workflow.connect([(sliding_glm_node, zstat_nipype_merge_node, [('out_z', 'in{}'.format(sliding_window_index+1))])])
    
    if betas:
        workflow.connect([(betas_nipype_merge_node, betas_file_merge_node, [('out', 'nii_paths')])])
    if tstat:
        workflow.connect([(tstat_nipype_merge_node, tstat_file_merge_node, [('out', 'nii_paths')])])
    if zstat:
        workflow.connect([(zstat_nipype_merge_node, zstat_file_merge_node, [('out', 'nii_paths')])])

    return workflow

def sliding_window_analysis(input_flist, output_path, seed, slidingstep, slidingwindow, betas=False, tstat=False, zstat=True, working_dir=None, n_procs=2):
    """
    Function to generate and run a sliding window analysis using multiple regression across several nii files. 
    Arguments:
        input_flist {str} -- Newline delimited text file containing one nii file per line. 
        output_path {str} -- Desired output path for the results to be saved in. Results are not sent to a nipype DataSink, simply saved in this folder.
        seed {str} -- Desired seed mask path.
        slidingstep {int} -- Number of TRs to increment the sliding window collection.
        slidingwindow {int} -- Number of TRs per window.
    
    Keyword Arguments:
        betas {bool} -- Toggle return of betas (parameter estimates). (default: {False})
        tstat {bool} -- Toggle return of tstats. (default: {False})
        zstat {bool} -- Toggle return of zstats. (default: {True})
        working_dir {type} -- Desired working directory for storage of intermediate files. Defaults to /tmp. (default: {None})
        n_procs {int} --  (default: {2})
    """

    nii_paths = parse_input_files(input_flist)
    binarized_seed = get_binarized_seed(seed)
    
    if working_dir is None:
        working_dir = '/tmp'

    workflow = Workflow(name='sliding_window_analysis', base_dir=working_dir)

    for file_idx, nii_file in enumerate(nii_paths):
        single_analysis = generate_sliding_window_map(nii_file, seed, slidingstep, slidingwindow, working_dir, output_path, identifier=str(file_idx).zfill(4), tstat=args.tstat, betas=args.bstat, zstat=args.zstat)
        workflow.add_nodes([single_analysis])

    workflow.run(plugin='MultiProc', plugin_args={
                                        'n_procs': args.n_procs,
                                        'crashfile_format': 'txt'})

if __name__ == '__main__':
    args = parse_args()
    sliding_window_analysis(args.input_flist, args.output_path, args.seed, args.slidingstep, args.slidingwindow, betas=args.bstat, tstat=args.tstat, zstat=args.zstat, n_procs=args.n_procs)


