Script to reproduce results of Lorenzo Pasquini's analysis of sliding window connectivity in the anterior insula.

# Installation
The script requires numpy, nipype, and nibabel. They can all be installed using pip or conda. 

conda install -c conda-forge nipype nibabel
conda install -c anaconda numpy

or,

pip install --user numpy nipype nibabel

This script has been tested and validated using the following programming environment:

Python 3.7
nipype 1.2.2
nibabel 2.5.0
numpy 1.16.4
FSL 6.0.1

On an Ubuntu workstation running Ubuntu 16.04.4 LTS and kernel 4.4.0-161-generic. 

The attached Singularity.snowflake file can be used to generate a Singularity image and some other convenience packages (matplotlib, scipy, scikit-learn etc.) that can be used to run the script as well. 

# Usage

The argparse interface will give some hints to the program usage with python sliding_window.py -h. Else, here is another explanation of the arguments. 

usage: sliding_window.py [-h] [--output_path OUTPUT_PATH]
                         [--slidingstep SLIDINGSTEP]
                         [--slidingwindow SLIDINGWINDOW] [--tstat] [--zstat]
                         [--bstat] [--workingdir WORKINGDIR]
                         [--n-procs N_PROCS]
                         input_flist seed

positional arguments:


  input_flist           Path of input file list, must be newline delimited
                        text file with one file path to a .nii or .nii.gz file
                        on each line.
                        
                        
  seed                  Path to the desired seed mask image. If the image is
                        not binary it will be assumed that all nonzero voxels
                        in the mask image are desired for timeseries
                        extraction.

optional arguments:
  -h, --help            show this help message and exit
  
  --output_path OUTPUT_PATH
                        Path to desired output directory. If the path does not
                        exist it will be created.
                        
  --slidingstep SLIDINGSTEP
                        Number of TRs to increment the sliding windows.
                        
  --slidingwindow SLIDINGWINDOW
                        Number of TRs to include in each sliding window
                        constituent analysis.
                        
  --tstat               Output and concatenate t-statistics from sliding
                        window analysis.
                        
  --zstat               Output and concatenate z-statistics from sliding
                        window analysis.
                        
  --bstat               Output and concatenate betas (parameter estimates)
                        from sliding window analysis.
                        
  --workingdir WORKINGDIR
                        Working directory to store temp nipype workflow files
                        and intermediates. Defaults to /tmp or TMPDIR (if
                        created).
                        
  --n-procs N_PROCS     Number of processes (not threads) to use for the
                        MultiProcessing.
